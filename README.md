# OpenML dataset: Crystal-System-Properties-for-Li-ion-batteries

https://www.openml.org/d/43433

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains data about the physical and chemical properties of the Li-ion silicate cathodes. These properties can be useful to predict the class of a Li-ion battery. These batteries can be classified on the basis of their crystal system. Three major classes of crystal system include: monoclinic, orthorhombic and triclinic. Go play out with the dataset to predict the battery classes with any classification algorithm you want!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43433) of an [OpenML dataset](https://www.openml.org/d/43433). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43433/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43433/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43433/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

